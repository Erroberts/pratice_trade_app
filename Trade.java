/**
 * Created by erobe01 on 12/4/2018.
 */
abstract class Trade {
    private String instrumentName;
    private  int quantityTraded;
    private double tradePrice;
    private String accountName;



    public Trade(String instrumentNameIN, int quantityTradedIN, double tradePriceIN, String accountNameIN) {
        instrumentName = instrumentNameIN;
        quantityTraded = quantityTradedIN;
        tradePrice = tradePriceIN;
        accountName = accountNameIN;
    }


    //private double optionPrice = tradePrice*quantityTraded;

    public String getAccountName() {
        return accountName;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public int getQuantityTraded() {
        return quantityTraded;
    }

    public double getTradePrice() {
        return tradePrice*quantityTraded;
    }

// public double getOptionPrice() {
     //   return optionPrice;


    public void setInstrumentName(String Name) {
        instrumentName = Name;
    }
    public void setQuantityTraded(int Traded){
        quantityTraded =Traded;
    }
    public void setTradePrice(double price){
        tradePrice = price;
    }
    public void setAccountName(String account){
        accountName = account;
    }

    @Override
    public String toString() {
        return this.accountName + " Trade{" +
                "instrumentName = '" + instrumentName +'\'' +
                ", quantityTraded = " + quantityTraded +
                ", positionPrice = $" + tradePrice + ", costOfTrade = $"+ tradePrice*quantityTraded;
    }
}
