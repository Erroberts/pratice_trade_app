/**
 * Created by erobe01 on 12/4/2018.
 */
public class PitTrade extends ElectronicTrade {
    private String traderName;

    public PitTrade(String instrumentNameIN, int quantityTradedIN, double tradePriceIN, String accountNameIN, String exchangeNameIN, String traderNameIN) {
        super(instrumentNameIN, quantityTradedIN, tradePriceIN, accountNameIN, exchangeNameIN);
        traderName = traderNameIN;
    }

    public String getTraderName() {
        return traderName;
    }

    public void setTraderName(String traderName) {
        this.traderName = traderName;
    }
}
