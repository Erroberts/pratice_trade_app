/* *
 * Created by erobe01 on 12/4/2018.
 */
import java.util.*;

public class WriteHere {
    public static void main(String[] args) {
        ArrayList myList = new ArrayList();

        HashMap<String, Double> myMap = new HashMap<>();
        double totalSUM;
        HashMap<String, Double> myMapAverge = new HashMap<>();

        ElectronicTrade myTest = new ElectronicTrade("WMT", 30, 80.7, "Kayston", "NYSE");
        System.out.println(myTest.toString());/** This is my test to make sure Object was created*/
        ElectronicTrade test2 = new ElectronicTrade("OCC", 10, 150, "Kayston", "CSE");
        ElectronicTrade test3 = new ElectronicTrade("OCC", 46, 150, "i.c. stars", "NYSE");
        ElectronicTrade test4 = new ElectronicTrade("Marvel", 100, 45, "Kayston", "CSE");
        ElectronicTrade test5 = new ElectronicTrade("Lenovo", 52, 10, "i.c. stars", "NYSE");
        ElectronicTrade test6 = new ElectronicTrade("VICI", 12, 50.5, "Kayston", "CSE");
        ElectronicTrade test7 = new ElectronicTrade("VICI", 25, 50.5, "Franklin", "NYSE");
        ElectronicTrade test8 = new ElectronicTrade("VICI", 5, 50.5, "i.c. stars", "NYSE");
        PitTrade test9 = new PitTrade("BAC", 40, 25.68, "Franklin", "NYSE", "John Doe");

        myMap.put(myTest.getInstrumentName(), myTest.getTradePrice());
        myMap.put(test2.getInstrumentName(), test2.getTradePrice());
        myMap.put(test3.getInstrumentName(), test3.getTradePrice());
        myMap.put(test4.getInstrumentName(), test4.getTradePrice());
        myMap.put(test5.getInstrumentName(), test5.getTradePrice());
        myMap.put(test6.getInstrumentName(), test6.getTradePrice());
        myMap.put(test7.getInstrumentName(), test7.getTradePrice());
        myMap.put(test8.getInstrumentName(), test8.getTradePrice());
        myMap.put(test9.getInstrumentName(), test9.getTradePrice());
        System.out.println(myMap);/** This is my test to make sure Object was created*/


/** This is the second hashmap containing names and averages*/
        double occAverage = (test2.costOfTrade + test3.costOfTrade) / 2;
        double ViciAverage = (test4.costOfTrade + test5.costOfTrade + test6.costOfTrade) / 3;
        myMapAverge.put(test2.getInstrumentName(), occAverage);
        myMapAverge.put(test6.getInstrumentName(), ViciAverage);
        myMapAverge.put(test9.getInstrumentName(), 25.68);
        myMapAverge.put(test5.getInstrumentName(), 10.0);
        myMapAverge.put(test4.getInstrumentName(), 45.0);
        myMapAverge.put(myTest.getInstrumentName(), 80.7);

        myList.add(myTest);
        myList.add(test2);
        myList.add(test3);
        myList.add(test4);
        myList.add(test5);
        myList.add(test6);
        myList.add(test7);
        myList.add(test8);
        myList.add(test9);
        System.out.println(myList);/** This is my test to make sure Object was created*/

        //  Arrays.stream(new List[]{occCashValueList}).average();
        //  Collections.sort(myList);
        totalSUM = (myTest.costOfTrade + test2.costOfTrade + test3.costOfTrade + test4.costOfTrade + test5.costOfTrade + test6.costOfTrade + test7.costOfTrade + test8.costOfTrade + test9.costOfTrade);
/** This is Starts printing out information*/


        System.out.println("This is the total sum of all the trades: $" + totalSUM);/** This is the total sum for all trades..*/
        System.out.println(myMap.keySet());/** This is prints out the list of keys in the Hash Map*/
//        for(String i: myMap.keySet()){
//            System.out.println("This is the average price for each Instrument: "+ i + " "+myTest.costOfTrade);}
        System.out.println("This is the average price for each Instrument: " + myMapAverge);

    }
}
