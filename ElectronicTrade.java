/**
 * Created by erobe01 on 12/4/2018.
 */
public class ElectronicTrade extends Trade /*E implements Comparable<E>, InterFace */{
    private String exchangeName;

    public double costOfTrade = getTradePrice() * getQuantityTraded();

    public ElectronicTrade(String instrumentNameIN, int quantityTradedIN, double tradePriceIN, String accountNameIN, String exchangeNameIN) {
        super(instrumentNameIN, quantityTradedIN, tradePriceIN, accountNameIN);
        exchangeName = exchangeNameIN;

    }


    public String getExchangeName() {
        return exchangeName;
    }

    public double getCostOfTrade() {
        return costOfTrade;
    }
}